from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains
import time
import os

chrome_options = webdriver.ChromeOptions()
chrome_options.binary_location = os.environ.get("GOOGLE_CHROME_BIN")
chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-dev-shm-usage")
chrome_options.add_argument("--no-sandbox")
driver = webdriver.Chrome(executable_path=os.environ.get("CHROMEDRIVER_PATH"), options=chrome_options)

try:
    driver.get("https://www.btn.co.id/")
    assert "Bank BTN Sahabat Keluarga Indonesia" in driver.title
    time.sleep(1)
    print("Laman Bank BTN sudah terbuka.")
    
    kursValas = driver.find_element(By.CSS_SELECTOR, ".btn-switch-money")
    actions = ActionChains(driver)
    actions.move_to_element(kursValas).perform()
    time.sleep(1)
    print("Pilihan Kurs Valas terbuka.")
    
    kursButton = driver.find_element(By.XPATH, "//a[normalize-space()='e-kurs konverter kalkulator']").click()
    time.sleep(1)
    print("Laman selanjutnya sudah dibuka!")
    time.sleep(1)
    driver.execute_script("window, scrollBy(0,1500);")
    time.sleep(1)
    
    inputKurs = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//input[@id='SourceNumber']")))
    inputKurs.clear()
    inputKurs.send_keys("5")
    time.sleep(1)
    
    chooseIDR = driver.find_element(By.XPATH, "//select[@class='form-control cbCurrencyTo']//option[@value='IDR']").click()
    time.sleep(1)
    hitungButton = driver.find_element(By.XPATH, "//input[@type='submit' and @value='hitung']").click()
    time.sleep(1)
    
    result = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//input[@id='Result']")))
    resultIDR = result.get_attribute("value")
    print(f"Hasil Konversi: {resultIDR}")
    
    assert resultIDR == "31.180", "Konversi tidak sesuai dengan yang diharapkan."
  
except NoSuchElementException as e:
    print("No element detected!", e)

except AssertionError as e:
    print("Assertion error!", e)
    time.sleep(1)
    
except Exception as e:
    print("An error occured: ", e)
    time.sleep(1)
    
finally:
    driver.quit
