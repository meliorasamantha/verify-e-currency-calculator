import requests

# Replace with the URL of your deployed Vercel function
vercel_url = "https://vercel.com/meliorasamantha/btn-test-script/Duim82ozscC2J4DZRKv5eq4aJWzh"

try:
    response = requests.get(vercel_url)

    if response.status_code == 200:
        print("Automation executed successfully.")
    else:
        print("Automation failed.")
        print("Response status code:", response.status_code)
        print("Response text:", response.text)

except requests.exceptions.RequestException as e:
    print("An error occurred while sending the request:", e)